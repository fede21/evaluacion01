<%-- 
    Document   : Index Evaluacion_1
    Created on : 30 abr. 2021, 16:03:25
    Author     : Federico Vial
--%>
<html>
<head>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 50%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 0px;
  padding-bottom:0px;
  text-align: center;
  background-color: #4CAF50;
  color: white;
}

.p1 {
  font-family: "Lucida Console", "Courier New", monospace;
}

</style>
</head>
<body>

    <h2 class="p1" align="center">Calculadora interes</h2>
    
    
   <form id="calc" method="get" action="ControladorCalculadora"> 
<table id="customers" align="center">
  <tr>
    <th></th>
    <th></th>
    
  </tr>
  <tr>
    <td> Capital: </td>
    <td><input type="number" name="capital" id="capital" placeholder="ingrese capital"></td>
    
  </tr>
  <tr>
    <td>Tasa interes: </td>
    <td><input type="number" name="ti" id="ti" placeholder="ingrese interes"> </td>
    
  </tr>
  <tr>
    <td>A�os: </td>
    <td><input type="number" name="a�os" id="a�os" placeholder="ingrese a�os"></td>
    
  </tr>
  
  
  <tr>
    <td><input type="reset" value="Borrar" align="center"> </td>
    <td><input type="submit" value="�Calcular!" align="center"></td>
    
  </tr>
  
</table>
</form>
</body>
</html>

