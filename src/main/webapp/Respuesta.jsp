<%-- 
    Document   : Respuesta Evaluacion_1
    Created on : 30 abr. 2021, 16:03:25
    Author     : Federico Vial
--%>

<%@page import="modelo.calculadora_interes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%   


calculadora_interes calcu =(calculadora_interes)request.getAttribute("calculadora_interes"); 

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado</title>
    </head>
    <body>
        
        
        <h2>El interes es de $<%= calcu.calcular() %></h2>
        
        
    </body>
</html>
